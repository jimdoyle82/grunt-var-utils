/*
 * grunt-var-utils
 * https://bitbucket.org/jimdoyle82/grunt-var-utils.git/overview
 *
 * Copyright (c) 2013 James Doyle
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  grunt.initConfig({

    // example of nested variables that we will retrieve with 'grunt.var_utils.getNestedConfigVariable'
    PARENT_VAR: {
      CHILD_VAR1: "this is a child variable"
      ,CHILD_VAR2: {
        CHILD_VAR2_A: "this is a child 2 levels deep"
      }
    }
    
    ,var_utils:{
      options: {
         includeJS: true
        ,includeSCSS: true
        ,addCommonConfigVars: true
        ,addCommonAngularJSConfigVars: true
      },
      rootDir:"./"
    }

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');


  // By default, lint and run all tests.
  grunt.registerTask('default', ['var_utils', 'customTask']);

  grunt.registerTask('customTask', function() {

    // see all vars
    // console.log( grunt.config.data );

    var GET_VAR = grunt.var_utils.getVar;

    console.log( GET_VAR("PARENT_VAR.CHILD_VAR1") );
    console.log( GET_VAR("PARENT_VAR.CHILD_VAR2.CHILD_VAR2_A") );

    // log an angular variable
    console.log( GET_VAR("DIRS.DEV") );
  });

};
