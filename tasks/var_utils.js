/*
 * grunt-var-utils
 * https://bitbucket.org/jimdoyle82/grunt-var-utils.git/overview
 *
 * Copyright (c) 2013 James Doyle
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  var PLUGIN_NAME = "grunt-var-utils";

  var type = function ( test ) {
      return ( {} ).toString.call( test ).match( /\s([a-zA-Z]+)/ )[ 1 ].toLowerCase();
    }
    ,getVar = function( name ) {

    var splitArr = name.split('.')
        ,nestedVar = grunt.config.data
        ,nameSegment;

    for( var i=0; i<splitArr.length; i++) {
      nameSegment = splitArr[i];
      nestedVar = nestedVar[ nameSegment ];
    }

    return nestedVar;
  }
  ,addCommonConfigVars = function( includeJS, includeSCSS, rootDir ) {

    var configObj = grunt.config.data
        ,packageJSONFile = grunt.file.readJSON('package.json');


    configObj.DIRS = configObj.DIRS || {};

    configObj.DIRS.DEV      = rootDir + 'development/';
    configObj.DIRS.PROD     = rootDir + 'production/';

    configObj.DIRS.DEV_EXT  = '<%= DIRS.DEV %>externals/';
    configObj.DIRS.DEV_BOW  = '<%= DIRS.DEV %>bower_components/';
    configObj.DIRS.DEV_IMG    = '<%= DIRS.DEV %>img/';
    configObj.DIRS.PROD_IMG   = '<%= DIRS.PROD %>img/';

    configObj.PKG_NAME        = packageJSONFile.name;
    configObj.PKG_BASE_NAME   = packageJSONFile.baseName;
    configObj.PKG_VERSION     = '.v' + packageJSONFile.version;
    configObj.PKG_AUTHOR      = packageJSONFile.author.name;
    configObj.BUILT_NAME      = '<%= PKG_NAME %>' + '<%= PKG_VERSION %>';
    configObj.BANNER_CONTENT  = '<%= PKG_NAME %> - <%= PKG_VERSION %> - ' + grunt.template.today("yyyy-mm-dd") + ' - <%= PKG_AUTHOR %>';

    if( includeJS === true ) {
      configObj.DIRS.DEV_JS     = '<%= DIRS.DEV %>js/'
      configObj.DIRS.PROD_JS      = '<%= DIRS.PROD %>js/'
      configObj.DIRS.PROD_JS_MIN    = '<%= DIRS.PROD_JS %>minified/'
      configObj.DIRS.PROD_JS_SPLIT  = '<%= DIRS.PROD_JS %>split/'
    }

    if( includeSCSS === true ) {
      configObj.DIRS.DEV_CSS      = '<%= DIRS.DEV %>css/';
      configObj.DIRS.DEV_SCSS     = '<%= DIRS.DEV %>scss/';
      configObj.DIRS.PROD_SCSS    = '<%= DIRS.PROD %>scss/';
      configObj.DIRS.PROD_CSS     = '<%= DIRS.PROD %>css/';
      configObj.DIRS.PROD_CSS_MIN   = '<%= DIRS.PROD %>css/minified/';
      configObj.DIRS.PROD_CSS_DEBUG = '<%= DIRS.PROD %>css/debug/';
    }
  }
  ,addCommonAngularJSConfigVars = function() {

    var configObj = grunt.config.data
        ,packageJSONFile = grunt.file.readJSON('package.json');

    configObj.DIRS = configObj.DIRS || {};
    
    configObj.DIRS.APP_CORE         = '<%= DIRS.DEV %>App/core/';
    configObj.DIRS.APP_HOME         = '<%= DIRS.DEV %>App/home/';
    configObj.DIRS.APP_DEV          = '<%= DIRS.DEV %>App/';
    configObj.DIRS.DEPLOY           = '<%= DIRS.PROD %>deploy/';
    configObj.DIRS.APP_DEPLOY       = '<%= DIRS.DEPLOY %>App/';
    configObj.DIRS.DEPLOY_IMG       = '<%= DIRS.DEPLOY %>img/';
    configObj.DIRS.DEPLOY_DEBUG_CSS = '<%= DIRS.DEPLOY %>css/debug/';
    configObj.DIRS.DEPLOY_MIN_CSS   = '<%= DIRS.DEPLOY %>css/minified/';
  }
  ,rootDirCheck = function( rootDir ) {

    if( !rootDir || rootDir && type( rootDir ) != "string" ) {
      grunt.log.error( "Error: " + PLUGIN_NAME + " 'rootDir' must be set and must be a string." );
      return false;
    }

    return true;
  };

  

  grunt.registerMultiTask('var_utils', 'Utils for common variable usages.', function() {

    var options = this.options({
           includeJS: false
          ,includeSCSS: false
          ,addCommonConfigVars: false
          ,addCommonAngularJSConfigVars: false
        })
        ,data = this.data
        ,rootDir;

    if( type(data) === 'string' ) rootDir = data;
    else                          rootDir = data.rootDir;

    // "rootDir" is a base for your variables
    if(  !rootDirCheck( rootDir )  ) return;

    if( options.addCommonConfigVars === true )          addCommonConfigVars( options.includeJS, options.includeSCSS, rootDir );
    if( options.addCommonAngularJSConfigVars === true ) addCommonAngularJSConfigVars( options.includeJS, options.includeSCSS, rootDir );

    /**
     * Adds a new object to the grunt object so it can be referenced at any time, such as 
     * in a custom task that does not have a config.
     */
    grunt.var_utils = {
      getVar: getVar
    }


  });

};
