# grunt-var-utils

> Utils for common variable usages.

## Getting Started
This plugin requires Grunt `0.4.x`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-var-utils --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-var-utils');
```

## The "var_utils" task

### Overview
In your project's Gruntfile, add a section named `var_utils` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  var_utils:{
    options: {
       includeJS: true
      ,includeSCSS: true
      ,addCommonConfigVars: true
      ,addCommonAngularJSConfigVars: true
    },
    rootDir:"./"
  }
});
```




### Options


#### options.addCommonConfigVars
Type: `Boolean`
Default value: `false`

Specify if you want common variables set.


#### options.addCommonAngularJSConfigVars
Type: `Boolean`
Default value: `false`

Specify if you want common variables set for AngularJS.


#### options.includeJS
Type: `Boolean`
Default value: `false`

Specify if you want common variables to include javascript variables.


#### options.includeSCSS
Type: `Boolean`
Default value: `false`

Specify if you want common variables to include Sass (SCSS) variables.




### Usage Examples

#### Default Options
In this example, the 'rootDir' is set to one level above the Grunt directory. Only 'getVar' will become available by default.

```js
grunt.initConfig({
  var_utils:{
    rootDir:"../"
  }
});
```

#### Custom Options
In this example, the 'rootDir' is set to two levels above the Grunt directory.
The options have all been set to true to include all featured variables on the main config object.

```js
grunt.initConfig({
  var_utils:{
      options: {
         addCommonConfigVars: true
        ,addCommonAngularJSConfigVars: true
        ,includeJS: true
        ,includeSCSS: true
      },
      rootDir:"../../"
    }
});
```


## Release History
- 0.1.1: Fixed 'rootDir' variable to be adjustable.
- 0.1.0: Initial port over from grunt.jimd.utils. 'getNestedConfigVariable' changed to 'getVar' and heavily simplified.
